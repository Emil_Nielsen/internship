Week 3:

This week we succesfully updated and installed new Octopus features and tentacles. The knowledge we acquired throughout the process, gave us the opportunity to also implement these features to the two new Jumphost servers.

I also continued my work with Elastic Agents and completed my list. Next week, I will be verifying that everything works as intended, and I will officially call that project finished. I've also started updating the windows servers, while during other projects, such as fixing logging issues, creating scripts to handle set issues and implementing different features to Elastic, such as monitoring applications and APM monitoring.
