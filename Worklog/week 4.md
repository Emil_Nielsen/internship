Week 4:

This week I started bu cleaning up in the IO Backlog. This meant going through tasks that were created by the IO team but not yet delegated. I fixed a multitude of issues which I felt comfortable tackling. This included ordering equipment, creating documentation and sorting application in different services.

This week I also got the task of creating and establising a connection to a server which was never implemented. This involved configuring Content Routes and Internal DNS. Basically, applying rules for the server to locate the correct DNS, which would only be accessible when connected to the company's network. This involved configuring the ADC. For this task, I also created documentation for future implementations. 
