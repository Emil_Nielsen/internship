Week 7:

This week I did a lot of troubleshooting. This mainly revolted around the office printer and it's connectivity with our local network, as it is suppose to have the functionality of scanning and sending it to your own e-mail. This didn't work, so I spent a long time during that.

Another project was cleaning. This involved cleaning our backlog for old tasks, which is something that could be done in the waiting time of other task. The task that was done, causing waiting time, was the restructuring of our password infrastructure, which consisted of slaving through every password, testing them and deleting if weren't necessary anymore. Of course, my knowledge of what every password stored was used for was limited, so while waiting for answers from different colleagues, I could clean.
