The first week:

During the first week, I have been taking care of IT-support, while also been starting to research and work with my project.
I've been working here for 2 months prior to internship, so the the IT-support part was basically me doing the same tasks as before the internship, which consisted of fixing windows updater, resetting PCs, off- and onboarding new and old colleges, etc. 

The other workload that I was tasked with this week, was researching capabilities of and solving issues in ElasticSearch. I worked with Logfiles and troubleshooted on many servers that weren't uploading files. I also installed agents on all the servers, which allowed the servers to be connected to Elastic. 

Ruben Bajo is also working here, and while during my own stuff, we also have a bigger project, where we will be creating two new jump host server, which we will later on have to migrate all the existing servers to.

Sup: Good introduction.
