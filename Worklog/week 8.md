Week 8:

This week I started the migration project to the Atlassian cloud. This meant I had to gather relevant information about our services, what plugins we were utilizing, and had to fix issues with the migration. The migration part of the project was handled by an external company, so I was basically the middle man, who had to oversee that everything went according to plan and to fix an issues popping up, while also be the main contact for our company.

The testing of the migration consisted of comparing the cloud version and the current version side by side, taking notes if anything was different. While waiting for the test migration to finish, I had time to do other tasks as well, such as finally fixing a printer and troubleshoot smaller issues here and there.

Next week, we will hopefully be starting an Elastic project, which could very well have something to do with a final project.
