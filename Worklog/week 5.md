Week 5:

This week I fixed issues regarding ELastic Agents and DNS. I finished enrolling agents on all servers and edited our list of servers to correctly reflect the servers that are in use. As I had gained experience in DNS, I started during tasks involving DNS connectivity and routing.

During the week I also cleaned up servers and configured services enabled on set servers for them to automatically remove unused files and releases. An example of set services i Ovtopus. I was also included in a bigger project, which was about importing confidential information form one service to another.
