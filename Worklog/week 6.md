Week 6:

This week I onboarded a new college, which involved of finding useable equipment and setting up the hardware and all the services the person would need. While doing this, I also handled smaller tasks here and there, such as troubleshooting printer set up and updating software.

I also started the bigger project I mentioned last week, which involves giving access to different services for the other company, and also starting to make things ready for import. A start-up meeting was scheduled, so the project is on hold for now.

The company will also be enrolling a new Password Management system, meaning the old structure needed to be tested and re-structured. This was started late in the week and would be continued next week.
