Week 9:

This week I continued my work with the migration project I am overseeing. Last week I tested the test migration and wrote down the issues that came with the migration, which we went over and corrected. After this, the preparation for the last test migration was finished, so that the final test migration, of a service called Bitbucket, was made and then tested during the week. This would continue for most of the days.

While this project was happening, another task also took some time. This was about a updating tool on one of the servers, which sometimes made some silent errors, and would need to be monitored. This would have to be through Elastic. Therefore, me and Ruben would need to figure out, how to separate exact logs in order to create a system which would alarm when a certain log popped up. This is still a work in progress.

Other tasks and projects was also done or started this week. We had a upstart meeting about Kubernetes and how we would be able to implement it with our currect setup. I will come back to this later during the week.
